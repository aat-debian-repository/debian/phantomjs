## Limitations

Unlike original "phantomjs" binary that is statically linked with modified
QT+WebKit, Debian package is built with system libqt5webkit5. Unfortunately
the latter do not have webSecurity extensions therefore "--web-security=no"
is expected to fail.

  https://github.com/ariya/phantomjs/issues/13727#issuecomment-155609276

---

Ghostdriver is crippled due to removed source-less pre-built blobs:

  src/ghostdriver/third_party/webdriver-atoms/*

Therefore all PDF functionality is broken.


### Headless mode

To achieve headless-ness upstream statically link PhantomJS with customised
QT + Webkit. We don't want to ship forks of those projects. It would be
great to eventually convince upstream to use standard libraries.

Meanwhile one can use "xvfb-run" from "xvfb" package:

    xvfb-run -a --server-args="-screen 0 640x480x16" phantomjs

Note that by default Xvfb starts at 8bpp (GLX doesn't work at 8bpp).
See also

  https://bugzilla.redhat.com/show_bug.cgi?id=904851

---

Another way to run PhantomJS in headless mode is by setting

    QT_QPA_PLATFORM="offscreen"

or running phantomjs with "-platform offscreen" options.
